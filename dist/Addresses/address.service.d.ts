import { Address } from './Address';
import { Repository, UpdateResult } from 'typeorm';
export declare class AddressService {
    private addressesRepository;
    constructor(addressesRepository: Repository<Address>);
    getAddresses(): Promise<Address[]>;
    getAddress(id: number): Promise<Address>;
    getAddressesBy(param: any): Promise<Address[]>;
    deleteAddress(id: string): Promise<void>;
    addAddress(address: Address): Promise<Address>;
    editAddress(addressID: number, address: Address): Promise<UpdateResult>;
}
