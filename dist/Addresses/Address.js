"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
const index_1 = require("typeorm/index");
const user_1 = require("../Users/user");
let Address = class Address {
    constructor(id, street, zipCode) {
        this._id = id;
        this._street = street;
        this._zipCode = zipCode;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    get street() {
        return this._street;
    }
    set street(value) {
        this._street = value;
    }
    get zipCode() {
        return this._zipCode;
    }
    set zipCode(value) {
        this._zipCode = value;
    }
    get users() {
        return this._users;
    }
    set users(value) {
        this._users = value;
    }
};
__decorate([
    index_1.PrimaryGeneratedColumn(),
    __metadata("design:type", Number)
], Address.prototype, "_id", void 0);
__decorate([
    index_1.Column(),
    __metadata("design:type", String)
], Address.prototype, "_street", void 0);
__decorate([
    index_1.Column(),
    __metadata("design:type", Number)
], Address.prototype, "_zipCode", void 0);
__decorate([
    index_1.OneToMany(type => user_1.User, user => user.address),
    __metadata("design:type", Array)
], Address.prototype, "_users", void 0);
Address = __decorate([
    index_1.Entity(),
    __metadata("design:paramtypes", [Number, String, Number])
], Address);
exports.Address = Address;
//# sourceMappingURL=Address.js.map