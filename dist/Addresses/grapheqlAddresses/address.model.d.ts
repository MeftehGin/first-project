import { User } from '../../Users/grapheqlUsers/user.model';
export declare class Address {
    id?: number;
    street?: string;
    zipCode?: number;
    users?: User[];
}
