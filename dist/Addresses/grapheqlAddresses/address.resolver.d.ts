import { AddressService } from '../address.service';
import { UserService } from '../../Users/user.service';
export declare class AddressResolver {
    private addressService;
    private userService;
    constructor(addressService: AddressService, userService: UserService);
    address(id: number): Promise<import("../Address").Address>;
    addresses(): Promise<import("../Address").Address[]>;
}
