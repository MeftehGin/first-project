"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressController = void 0;
const common_1 = require("@nestjs/common");
const address_service_1 = require("./address.service");
const Address_1 = require("./Address");
let AddressController = class AddressController {
    constructor(addressService) {
        this.addressService = addressService;
    }
    async getAddresses() {
        const address = await this.addressService.getAddresses();
        return address;
    }
    async getAddress(addressID) {
        let address;
        this.addressService.getAddress(addressID).then(function (result) {
            address = result;
        });
        return await this.addressService.getAddress(addressID);
    }
    async addAddress(address) {
        return await this.addressService.addAddress(address);
    }
    async deleteUser(addressID) {
        const address = await this.addressService.deleteAddress(addressID);
        return address;
    }
    async editUser(addressID, address) {
        const userUpdated = await this.addressService.editAddress(addressID, address);
        return userUpdated;
    }
};
__decorate([
    common_1.Get(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AddressController.prototype, "getAddresses", null);
__decorate([
    common_1.Get(':addressID'),
    __param(0, common_1.Param('addressID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AddressController.prototype, "getAddress", null);
__decorate([
    common_1.Post(),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Address_1.Address]),
    __metadata("design:returntype", Promise)
], AddressController.prototype, "addAddress", null);
__decorate([
    common_1.Delete(':addressID'),
    __param(0, common_1.Param('addressID')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AddressController.prototype, "deleteUser", null);
__decorate([
    common_1.Put(':addressID'),
    __param(0, common_1.Param('addressID')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Address_1.Address]),
    __metadata("design:returntype", Promise)
], AddressController.prototype, "editUser", null);
AddressController = __decorate([
    common_1.Controller('address'),
    __metadata("design:paramtypes", [address_service_1.AddressService])
], AddressController);
exports.AddressController = AddressController;
//# sourceMappingURL=address.controller.js.map