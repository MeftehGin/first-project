import { User } from '../Users/user';
export declare class Address {
    private _id;
    private _street;
    private _zipCode;
    private _users;
    constructor(id: number, street: string, zipCode: number);
    get id(): number;
    set id(value: number);
    get street(): string;
    set street(value: string);
    get zipCode(): number;
    set zipCode(value: number);
    get users(): User[];
    set users(value: User[]);
}
