import { AddressService } from './address.service';
import { Address } from './Address';
export declare class AddressController {
    private addressService;
    constructor(addressService: AddressService);
    getAddresses(): Promise<Address[]>;
    getAddress(addressID: any): Promise<Address>;
    addAddress(address: Address): Promise<Address>;
    deleteUser(addressID: any): Promise<void>;
    editUser(addressID: any, address: Address): Promise<import("typeorm").UpdateResult>;
}
