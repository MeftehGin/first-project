import { Address } from '../../Addresses/Address';
export declare class User {
    id: number;
    firstName?: string;
    lastName?: string;
    address: Address;
}
