import { Address } from '../Addresses/Address';
export declare class User {
    private _id;
    private _firstName;
    private _lastName;
    private _age;
    private _address;
    private _create_at;
    private _updated_at;
    private _deleted_at;
    constructor(id: number, firstName: string, lastName: string, age: number, address: Address);
    get id(): number;
    set id(value: number);
    get firstName(): string;
    set firstName(value: string);
    get lastName(): string;
    set lastName(value: string);
    get age(): number;
    set age(value: number);
    get address(): Address;
    set address(value: Address);
    get create_at(): Date;
    set create_at(value: Date);
    get updated_at(): Date;
    set updated_at(value: Date);
    get deleted_at(): Date;
    set deleted_at(value: Date);
}
