export declare class UserRepositoryFake {
    create(): void;
    save(): Promise<void>;
    remove(): Promise<void>;
    findOne(): Promise<void>;
}
