import { User } from './user';
import { Repository, UpdateResult } from 'typeorm';
export declare class UserService {
    private usersRepository;
    constructor(usersRepository: Repository<User>);
    getUsers(): Promise<User[]>;
    getUser(id: number): Promise<User>;
    getUserBy(param: any): Promise<User[]>;
    deleteUser(id: string): Promise<void>;
    addUser(user: User): Promise<User>;
    editUser(userID: number, user: User): Promise<UpdateResult>;
}
