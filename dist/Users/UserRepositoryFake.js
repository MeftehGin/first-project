"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRepositoryFake = void 0;
class UserRepositoryFake {
    create() { }
    async save() { }
    async remove() { }
    async findOne() { }
}
exports.UserRepositoryFake = UserRepositoryFake;
//# sourceMappingURL=UserRepositoryFake.js.map