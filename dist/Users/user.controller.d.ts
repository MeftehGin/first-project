import { UserService } from './user.service';
import { User } from './user';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    getUsers(): Promise<User[]>;
    getUser(userID: any): Promise<User>;
    addBook(user: User): Promise<User>;
    deleteUser(userID: any): Promise<void>;
    editUser(userID: any, user: User): Promise<import("typeorm").UpdateResult>;
}
