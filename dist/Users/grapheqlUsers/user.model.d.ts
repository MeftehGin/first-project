import { Address } from '../../Addresses/grapheqlAddresses/address.model';
export declare class User {
    id?: number;
    firstName?: string;
    lastName?: string;
    address?: Address;
}
