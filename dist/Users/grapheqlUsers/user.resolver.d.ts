import { UserService } from '../user.service';
import { AddressService } from '../../Addresses/address.service';
export declare class UserResolver {
    private userService;
    private addressService;
    constructor(userService: UserService, addressService: AddressService);
    user(id: number): Promise<import("../user").User>;
    users(): Promise<import("../user").User[]>;
    address(user: any): Promise<import("../../Addresses/Address").Address[]>;
}
