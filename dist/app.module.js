"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
require("reflect-metadata");
const typeorm_1 = require("@nestjs/typeorm");
const address_module_1 = require("./Addresses/address.module");
const user_module_1 = require("./Users/user.module");
const dotenv = require("dotenv");
const graphql_1 = require("@nestjs/graphql");
const nest_keycloak_connect_1 = require("nest-keycloak-connect");
const microservices_1 = require("@nestjs/microservices");
dotenv.config();
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            nest_keycloak_connect_1.KeycloakConnectModule.register({
                authServerUrl: 'http://localhost:8090/auth',
                realm: 'FirstDomaine',
                clientId: 'First-App',
                secret: '8020326d-c0fe-469f-90a3-2981ff14687c',
            }),
            microservices_1.ClientsModule.register([
                {
                    name: 'HELLO_SERVICE', transport: microservices_1.Transport.RMQ,
                    options: {
                        urls: ['amqp://guest:guest@rabbitMQ:5672'],
                        queue: 'user-messages',
                        queueOptions: {
                            durable: false
                        },
                    },
                },
            ]),
            user_module_1.UserModule,
            address_module_1.AddressModule,
            graphql_1.GraphQLModule.forRoot({
                autoSchemaFile: true,
                context: ({ req }) => ({ headers: req.headers })
            }),
            typeorm_1.TypeOrmModule.forRoot({
                type: "postgres",
                url: "postgres://root:password@db:5432/db",
                autoLoadEntities: true,
                synchronize: true,
            }),
        ],
        controllers: [app_controller_1.AppController],
        providers: [
            app_service_1.AppService,
        ],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map