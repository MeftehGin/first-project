import { Controller, Get, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientProxy } from '@nestjs/microservices';
import { Message } from './message.event';

@Controller()
export class AppController {
  constructor(@Inject('HELLO_SERVICE') private readonly client: ClientProxy) { }

  @Get()
  getHello() {
    this.client.emit<any>('message_printed', new Message('Hello World'));
    return 'Hello World printed';
  }

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  // @EventPattern('message_printed')
  // async handleMessagePrinted(data: Record<string, unknown>) {
  //   console.log(data.text);
  // }
}
