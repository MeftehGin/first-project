import { Injectable } from '@nestjs/common';
import { User } from './user';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository,UpdateResult } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  getUsers(): Promise<User[]> {
    return this.usersRepository.find();
  }

  getUser(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  getUserBy(param: any): Promise<User[]> {
    return this.usersRepository.find(param);
  }

  async deleteUser(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }

  addUser(user : User): Promise<User>{
    return this.usersRepository.save(user);
  }

  editUser(userID : number, user : User): Promise<UpdateResult>{
    return this.usersRepository.update(userID,user);
  }
}
