import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Address } from '../../Addresses/grapheqlAddresses/address.model';

@ObjectType()
export class User {
  @Field(type => Int,{ nullable: true })
  id?: number;

  @Field({ nullable: true })
  firstName?: string;

  @Field({ nullable: true })
  lastName?: string;

  @Field(_type=>Address,{ nullable: true })
  address?: Address;

}