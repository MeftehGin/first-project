import { Args, ID, Int, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { UserService } from '../user.service';
import { User } from './user.model';
import { AddressService } from '../../Addresses/address.service';

@Resolver(of=>User)
export class UserResolver {

  constructor(
    private userService: UserService,
    private addressService: AddressService
  ) {}

  @Query(returns => User)
  async user(@Args('id', { type: () => Int }) id: number) {
    return  this.userService.getUser(id);
  }

  @Query(() => [User])
  async users(){
    return this.userService.getUsers();
  }

  @ResolveField()
  async address(@Parent() user) {
    const { id } = user;
    return this.addressService.getAddressesBy({user:id});
  }
}