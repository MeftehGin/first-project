import { Body, Controller, Delete, Get, Param, Post, Put, Query } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user';

@Controller('user')
export class UserController {

  constructor(private userService : UserService) { }

  @Get()
  public async getUsers() {
    const users = await this.userService.getUsers();
    return users;
  }

  @Get(':userID')
  async getUser(@Param('userID') userID) {
    const user = await this.userService.getUser(userID);
    return user;
  }

  @Post()
  async addBook(@Body() user: User) {
    const userNew = await this.userService.addUser(user);
    return userNew;
  }

  @Delete(':userID')
  async deleteUser(@Param('userID') userID) {
    const user = await this.userService.deleteUser(userID);
    return user;
  }

  @Put(':userID')
  async editUser(@Param('userID') userID, @Body() user : User){
    const userUpdated = await this.userService.editUser(userID,user);
    return userUpdated;

  }

}
