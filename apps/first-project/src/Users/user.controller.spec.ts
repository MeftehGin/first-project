
import { Test, TestingModule } from '@nestjs/testing';
import { UserRepositoryFake } from './UserRepositoryFake';
import { UserService } from './user.service';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user';
import { Repository } from 'typeorm/index';


describe('UserService', () => {
  let userService: UserService;
  let userRepository: Repository<User>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: UserRepositoryFake,
        },
      ],
    }).compile();

    userService = module.get(UserService);
    userRepository = module.get(getRepositoryToken(User));
  });

  describe('getUsers', () => {
    it('should return an array of Users', async () => {
      const result = Promise.resolve([]);

      jest.spyOn(userService, 'getUsers').mockImplementation(() => result);
      console.log(userService.getUsers())
      expect(await userService.getUsers())//.toBe(result);
    });
  });
});