import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    DeleteDateColumn,
    ManyToOne,
    JoinColumn
} from 'typeorm';

import { Address } from '../Addresses/Address';

@Entity()
export class User {

    @PrimaryGeneratedColumn() private _id: number;

    @Column() private _firstName: string;

    @Column() private _lastName: string;

    @Column() private _age: number;


    @ManyToOne(type => Address,address => address.users,{
        eager : true
    })
    // @JoinColumn({name:"idAddress",referencedColumnName:"id"})
    private _address: Address;

    @CreateDateColumn() private _create_at: Date

    @UpdateDateColumn() private _updated_at: Date

    @DeleteDateColumn() private _deleted_at : Date


    constructor(id: number, firstName: string, lastName: string, age: number, address: Address) {
        this._id = id;
        this._firstName = firstName;
        this._lastName = lastName;
        this._age = age;
        this._address = address;
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get age(): number {
        return this._age;
    }

    set age(value: number) {
        this._age = value;
    }

    get address(): Address {
        return this._address;
    }

    set address(value: Address) {
        this._address = value;
    }

    get create_at(): Date {
        return this._create_at;
    }

    set create_at(value: Date) {
        this._create_at = value;
    }

    get updated_at(): Date {
        return this._updated_at;
    }

    set updated_at(value: Date) {
        this._updated_at = value;
    }

    get deleted_at(): Date {
        return this._deleted_at;
    }

    set deleted_at(value: Date) {
        this._deleted_at = value;
    }
}
