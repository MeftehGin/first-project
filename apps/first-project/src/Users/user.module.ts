import { forwardRef, Module } from '@nestjs/common';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './user';
import { UserResolver } from './grapheqlUsers/user.resolver';
import { AddressModule } from '../Addresses/address.module';
import { UserController } from './user.controller';

@Module({
  imports: [
    forwardRef(() => AddressModule),
    TypeOrmModule.forFeature([User])],
  controllers: [UserController],
  providers: [UserService,UserResolver],
  exports:[UserService]
})
export class UserModule {}
