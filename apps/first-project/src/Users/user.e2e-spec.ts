import * as request from 'supertest';
import { Test } from '@nestjs/testing';

import { INestApplication } from '@nestjs/common';
import { UserModule } from './user.module';
import { UserService } from './user.service';
import { Repository } from 'typeorm/index';
import { User } from './user';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserRepositoryFake } from './UserRepositoryFake';

describe('User', () => {
  let app: INestApplication;
  const userService = { findAll: () => ['test'] };
  let userRepository: Repository<User>;

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [UserModule],
      providers: [
        UserService,
        {
          provide: getRepositoryToken(User),
          useClass: UserRepositoryFake,
        },
      ],
    })
      .overrideProvider(UserService)
      .useValue(userService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET user`, () => {
    return request(app.getHttpServer())
      .get('/user')
      .expect(200)
      .expect({
        data: userService.findAll(),
      });
  });

  afterAll(async () => {
    await app.close();
  });
});