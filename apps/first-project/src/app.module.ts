import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import "reflect-metadata";
import { TypeOrmModule } from '@nestjs/typeorm';
import { AddressModule } from './Addresses/address.module';
import { UserModule } from './Users/user.module';
import * as dotenv from 'dotenv';
import { GraphQLModule } from '@nestjs/graphql';
import { KeycloakConnectModule, ResourceGuard } from 'nest-keycloak-connect';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from './auth.guard';
import { Transport, ClientsModule } from '@nestjs/microservices';


dotenv.config(); // load variables environment from .env file to process.env file

@Module({
  imports: [
    KeycloakConnectModule.register({
      authServerUrl: 'http://localhost:8090/auth',
      realm: 'FirstDomaine',
      clientId: 'First-App',
      secret: '8020326d-c0fe-469f-90a3-2981ff14687c',
    }),

    ClientsModule.register([
      {
        name: 'HELLO_SERVICE', transport: Transport.RMQ,
        options: {
          urls: ['amqp://guest:guest@rabbitMQ:5672'],
          queue: 'user-messages',
          queueOptions: {
            durable: false
          },
        },
      },
    ]),
    UserModule,
    AddressModule,
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      context: ({req})=>({headers:req.headers})
    }),
    TypeOrmModule.forRoot({
      type : "postgres",
      url: "postgres://root:password@db:5432/db",
      autoLoadEntities: true,
      synchronize: true,
    }),
  ],
  controllers: [AppController],
  providers: [
    AppService,
    // {
    //   provide: APP_GUARD,
    //   useClass: AuthGuard,
    // },
    // {
    //   provide: APP_GUARD,
    //   useClass: ResourceGuard,
    // },
  ],
})
export class AppModule {}
/*


        TypeOrmModule.forRoot({
      type : 'postgres',
      host: '172.18.0.2',
      port: 5432,
      username: 'root',
      password: 'password',
      database: 'db',
      autoLoadEntities: true,
      //entities: [Address,User],
      synchronize: false,

      // port: parseInt(process.env['DATABASE_PORT ']),
      // username: process.env['DATABASE_USERNAME'],
      // password: process.env['DATABASE_PASSWORD'],
      // database: process.env['DATABASE_NAME'],
    }),
 */