import { forwardRef, Module } from '@nestjs/common';
import { AddressController } from './address.controller';
import { AddressService } from './address.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Address } from './Address';
import { AddressResolver } from './grapheqlAddresses/address.resolver';
import { UserModule } from '../Users/user.module';


@Module({
  imports: [
    forwardRef(() => UserModule),
    TypeOrmModule.forFeature([Address])],
  controllers: [AddressController],
  providers: [AddressService,AddressResolver],
  exports : [AddressService]
})
export class AddressModule {}
