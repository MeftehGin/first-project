import { Field, Int, ObjectType } from '@nestjs/graphql';
import { User } from '../../Users/grapheqlUsers/user.model';

@ObjectType()
export class Address {
  @Field(type => Int,{ nullable: true })
  id?: number;

  @Field({ nullable: true })
  street?: string;

  @Field(type => Int,{ nullable: true })
  zipCode?: number;

  @Field(_type=>[User],{ nullable: true })
  users?:User[];
}