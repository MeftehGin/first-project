import { Args, Int, Parent, Query, ResolveField, Resolver } from '@nestjs/graphql';
import { AddressService } from '../address.service';
import { Address } from './address.model';
import { UserService } from '../../Users/user.service';
import { User } from '../../Users/grapheqlUsers/user.model';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from '../../auth.guard';

@Resolver(of=>Address)
export class AddressResolver {

  constructor(
    private addressService: AddressService,
    private userService: UserService
  ) {}

  @Query(returns => Address)
  async address(@Args('id', { type: () => Int }) id: number) {
    return this.addressService.getAddress(id);
  }

  // @UseGuards(AuthGuard)
  @Query(returns => [Address])
  async addresses(){
    return this.addressService.getAddresses();
  }

  // @ResolveField(()=>[User])
  // async users(@Parent() address): Promise<User[]> {
  //   const { id } = address;
  //   return await this.userService.getUserBy({ address: id });
  // }
}