import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { AddressService } from './address.service';
import { Address } from './Address';

@Controller('address')
export class AddressController {
  constructor(private addressService : AddressService) { }

  @Get()
  public async getAddresses() {
    const address = await this.addressService.getAddresses();
    return address;
  }

  @Get(':addressID')
  async getAddress(@Param('addressID') addressID) {
   let address : Address;//= new Address(0, '', 0);

    this.addressService.getAddress(addressID).then(function(result) {
      // here you can use the result of promiseB
      address = result;
    });
//console.log("users "+address.users)
    return await this.addressService.getAddress(addressID);
  }

  @Post()
  async addAddress(@Body() address: Address) {
    return await this.addressService.addAddress(address);
  }

  @Delete(':addressID')
  async deleteUser(@Param('addressID') addressID) {
    const address = await this.addressService.deleteAddress(addressID);
    return address;
  }

  @Put(':addressID')
  async editUser(@Param('addressID') addressID, @Body() address : Address){
    const userUpdated = await this.addressService.editAddress(addressID,address);
    return userUpdated;

  }
}
