import { Injectable } from '@nestjs/common';
import { Address } from './Address';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository,UpdateResult } from 'typeorm';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(Address)
    private addressesRepository: Repository<Address>,
  ) {}

  getAddresses(): Promise<Address[]> {
    //console.log("address "+ this.addressesRepository.find({ users:[] }))
    return this.addressesRepository.find();
  }

  getAddress(id: number): Promise<Address> {
    return this.addressesRepository.findOne(id)
  }

  getAddressesBy(param: any): Promise<Address[]> {
    return this.addressesRepository.find(param);
  }

  async deleteAddress(id: string): Promise<void> {
    await this.addressesRepository.delete(id);
  }

  addAddress(address:Address):Promise<Address>{
    return this.addressesRepository.save(address);
  }

  editAddress(addressID : number, address : Address): Promise<UpdateResult>{
    return this.addressesRepository.update(addressID,address);
  }

}
