import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm/index';
import { User } from '../Users/user';


@Entity()
export class Address{

  @PrimaryGeneratedColumn() private _id: number;

  @Column() private _street: string;

  @Column() private _zipCode: number;

  @OneToMany(type => User,user => user.address)
  private _users: User[];


  constructor(id: number, street: string, zipCode: number) {
    this._id = id;
    this._street = street;
    this._zipCode = zipCode;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get street(): string {
    return this._street;
  }

  set street(value: string) {
    this._street = value;
  }

  get zipCode(): number {
    return this._zipCode;
  }

  set zipCode(value: number) {
    this._zipCode = value;
  }

  get users(): User[] {
    return this._users;
  }

  set users(value: User[]) {
    this._users = value;
  }
}