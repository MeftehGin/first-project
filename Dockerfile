FROM node:12.13.0
WORKDIR /usr/src/app
COPY . .
RUN npm install
EXPOSE 3000:3000
VOLUME .:/usr/src/app
CMD ["npm", "run", "debug" ]
#RUN npm run build
#RUN npm run start:dev